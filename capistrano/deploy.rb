# -*- encoding : utf-8 -*-
# config valid only for Capistrano 3.1
lock '3.1.0'

set :weblication, 'Kivu'
set :repo_url, 'https://USERNAME:PASSWORD@bitbucket.org/newxcombr/ecommerce.git'
set :user, 'root'
set :port, 22

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

# Default deploy_to directory is /var/www/my_app
set :deploy_to, '/var/apps/ecommerce'


# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{config/database.yml}

# Default value for linked_dirs is []
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5
unicorn_pid = "#{deploy_to}/tmp/pids/unicorn.pid"
unicorn_rails = "unicorn_rails"

#PIDS
#pid_queue_unverified = "#{deploy_to}/tmp/pids/queue_unverified.pid"

# Adicione aqui o IP  do servidor de producao
role :web, "IP_SERVIDOR"

namespace :deploy do

  task :restart do
    invoke 'deploy:stop'
    invoke 'deploy:start'
  end

  task :start do
    on roles(:web) do
      execute "cd #{deploy_to} && bundle exec rake assets:precompile RAILS_ENV=production"
      execute "cd #{deploy_to} && bundle exec #{unicorn_rails} -p 3001 -c #{deploy_to}/config/unicorn.conf.rb -D --env production"
    end
  end

  desc 'Restart application'
  task :stop do
    on roles(:web) do
      execute "kill -s QUIT `cat #{unicorn_pid}`"
    end
  end

  after :stop, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  task :compile_assets do
    on roles(:web) do
      execute "cd #{deploy_to} && bundle exec rake assets:precompile RAILS_ENV=production"
    end
  end

  #  desc "Zero-downtime restart of Unicorn"
  #  task :restart do
  #    on roles(:web), in: :sequence, wait: 5 do
  #      execute "kill -s HUP `cat #{unicorn_pid}`"
  #    end
  #  end

  task :update do
    invoke 'deploy:update_code'
    invoke 'deploy:bundle'
    invoke 'deploy:migrate'
    invoke 'deploy:restart'
  end

  task :quick do
    invoke 'deploy:update_code'
    invoke 'deploy:restart'
  end

  task :bundle do
    on roles(:web), in: :sequence, wait: 5 do
      #execute "cd #{deploy_to} && bundle install --path vendor/gems --binstubs"
      execute "cd #{deploy_to} && bundle install"
    end
  end

  task :migrate do
    on roles(:web), in: :sequence, wait: 5 do
      execute "cd #{deploy_to} && bundle exec rake db:migrate RAILS_ENV=production"
    end
  end

  task :update_code do
    on roles(:web), in: :sequence, wait: 5 do
      execute "rm -rf #{deploy_to}/Gemfile.lock"
      execute "rm -rf #{deploy_to}/db/schema.rb"
      execute "cd #{deploy_to} && git stash"
      execute "cd #{deploy_to} && git pull"
      execute "cp #{deploy_to}/config/database.remote.yml #{deploy_to}/config/database.yml"
      #execute "cp #{deploy_to}/crontab /etc/crontab"
    end

    #invoke 'deploy:compile_assets'
  end

end
