COMO UTILIZAR O CAPISTRANO

1) Parando a aplicação

#> cap production deploy:stop

2) Iniciando a aplicação

#> cap production deploy:start

3) Reiniciando a aplicação

#> cap production deploy:restart

4) Deploy completo
    - atualiza aplicacao no servidor de producao
    - atualiza as gems
    - efetua a migração (migrate)
    - reinicia a aplicacao

#> cap production deploy:update

5) Deploy rápido
    - atualiza aplicacao no servidor de producao
    - reinicia a aplicacao

#> cap production deploy:quick
