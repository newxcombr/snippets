# -*- encoding : utf-8 -*-
# Unicorn says to use at least one worker process per core (particularly on dedicated servers)
worker_processes 8
APP_ROOT = File.dirname(File.dirname(File.expand_path(__FILE__)))

# The root directory of you app
working_directory APP_ROOT

# Port that worker processes listen on
# This can also be a unix socket
listen 3001, :tcp_nopush => true

# Location of master process PID file
pid "#{APP_ROOT}/tmp/pids/unicorn.pid"

# Location of stderr/stdout logs
stderr_path "#{APP_ROOT}/log/unicorn.stderr.log"
stdout_path "#{APP_ROOT}/log/unicorn.stdout.log"

before_fork do |server, worker|
  # a .oldbin file exists if unicorn was gracefully restarted with a USR2 signal
  # we should terminate the old process now that we're up and running
  old_pid = "#{APP_ROOT}/tmp/pids/unicorn.pid.oldbin"
  if File.exists?(old_pid)
    begin
      Process.kill("QUIT", File.read(old_pid).to_i)
    rescue Errno::ENOENT, Errno::ESRCH
      # someone else did our job for us
    end
  end
end

# Force the bundler gemfile environment variable to
# reference the Сapistrano "current" symlink
before_exec do |_|
  ENV["BUNDLE_GEMFILE"] = File.join(APP_ROOT, 'Gemfile')
end

# combine REE with "preload_app true" for memory savings
# http://rubyenterpriseedition.com/faq.html#adapt_apps_for_cow
preload_app true
GC.respond_to?(:copy_on_write_friendly=) and
GC.copy_on_write_friendly = true
